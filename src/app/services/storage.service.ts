import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class StorageService {

  preferenze: Object = {};

  constructor() {}
  
  getData(key: string): any {
    this.preferenze = JSON.parse(localStorage.getItem('preferenze'));
    if (this.preferenze==null){
      localStorage.setItem('preferenze', JSON.stringify({}));
      return null;
    }
    return this.preferenze[key] ? this.preferenze[key] : null;
  }

  setData(key: string, data: any) {
    this.preferenze = JSON.parse(localStorage.getItem('preferenze'));
    this.preferenze[key]=data;
    localStorage.setItem('preferenze', JSON.stringify(this.preferenze));
  }

}
