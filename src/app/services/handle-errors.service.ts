import { Injectable } from '@angular/core';
import { AppError } from './AppError';
import { TranslateService } from '@ngx-translate/core';
import{AlertService} from '../services/alert.service'
import{ButtonComponent} from '../button/button.component'


@Injectable({
  providedIn: 'root'
})
export class HandleErrorsService {
  
  constructor(private translate: TranslateService, /*private alertService: AlertService, private buttonComponent: ButtonComponent*/) {}
  
  handle(err, ambito: string):AppError {
    console.log(err);
    let appError = new AppError();
    appError.code = err.status;
    
    // tanta logica
    if (err.status == 500){
      appError.title = this.translate.instant("AREAS."+ambito.toUpperCase());
      appError.message = this.translate.instant("ERRORS."+ambito.toUpperCase()+"."+err.status) + "\n\n"+err.message+"\n\n"+err.error.message;    // ERRORS[ambito.touppercase][err.status]
      appError.level = "ERROR";    
      
      }   
    return appError;
  }
}
