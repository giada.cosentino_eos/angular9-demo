import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
/*
  questo service contatta il BE e ci ritorna le informazioni ricevute
*/

@Injectable({
  providedIn: 'root'
})
export class GenerateErrorService {

  constructor(private http: HttpClient) { }

  saluta (): void {
    alert('Ciao');
  }

  call(code: string) {
    return this.http.get<any>(`http://localhost:8088/api/${code}`)
  }
}
