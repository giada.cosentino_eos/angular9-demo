import { Component, OnInit, Output, Input, OnChanges, SimpleChanges } from '@angular/core';
import { EventEmitter } from 'events';
import { AppError } from '../services/AppError';
import { SharedServicesService } from '../services/shared-services.service';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit, OnChanges {

  active: boolean = false;
  titolo: string = "";
  descrizione: string = "";

 

  //@Input()
  data: AppError;



  constructor(private sharedServices: SharedServicesService) { }
  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes)
    if (changes?.data?.currentValue)
      this.showError(changes?.data?.currentValue)
  }



  ngOnInit(): void {
    this.sharedServices.sharedMessage.subscribe(objErr => {
      console.log("appAlert", objErr)     
      this.showError(objErr);
    });
  }

  showError(err: AppError) {
    if (err){
      this.data = err;
      this.titolo = err.title;
      this.descrizione = err.message;
      this.active = true;  
    }      
  }

  close() {
    this.active = false;
  }


}
